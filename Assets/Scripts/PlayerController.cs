﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [System.Serializable]
    public class Stats
    {
        public int Str;
        public int Con;
        public int Dex;
    }

    [System.Serializable]
    public class GUIElements
    {
        public Slider HealthSlider;
        public Slider ExpSlider;

        public Text LevelUpText;
        public Text LevelUpTextStats;
        public Text ExpText;
        public Text HealthText;

        public Text CharPointsTextSTR;
        public Text CharPointsTextDEX;
        public Text CharPointsTextCON;
    }

    public Stats CharacterPoints;
    public GUIElements GuiElements;

    public bool WeaponActive;

    private GameObject[] Enemies;




    [HideInInspector]
    public HealthController MyHealthAttributes;

    [HideInInspector]
    public WeaponController wepController;

    public float CurrentExp;
    public float CurrentLevel;

    public bool CanMove;

    void Awake()
    {
        Enemies = GameObject.FindGameObjectsWithTag("Enemy");

        MyHealthAttributes = gameObject.GetComponent<HealthController>();

        GuiElements.LevelUpText.enabled = false;
        GuiElements.LevelUpTextStats.enabled = false;

        MyHealthAttributes.MaxHealth = CharacterPoints.Con * 10;
        MyHealthAttributes.Health = MyHealthAttributes.MaxHealth;
    }

    // Use this for initialization
    void Start()
    {
    }

    void Update()
    {
        if (wepController == null)
        {
            wepController = GameObject.FindGameObjectWithTag("Player weapon").GetComponent<WeaponController>();
        }


        Controll();
        LookAtMouse();
        CheckLevelUp();
        UpdateGUI();


    }

    void UpdateGUI()
    {
        GuiElements.CharPointsTextSTR.text = "STR: " + CharacterPoints.Str;
        GuiElements.CharPointsTextDEX.text = "DEX: " + CharacterPoints.Dex;
        GuiElements.CharPointsTextCON.text = "CON: " + CharacterPoints.Con;
        GuiElements.ExpText.text = CurrentExp + " \\ " + (CurrentLevel * 100) + " (" + (CurrentExp / (CurrentLevel * 100) * 100) + "%)";
        GuiElements.HealthText.text = MyHealthAttributes.Health + " \\ " + MyHealthAttributes.MaxHealth;
    }

    void CheckLevelUp()
    {
        if (CurrentExp > CurrentLevel * 100)
        {
            CurrentExp -= CurrentLevel * 100;
            CurrentLevel++;

            var strGain = Random.Range(1, 4);
            var dexGain = Random.Range(1, 4);
            var conGain = Random.Range(1, 4);

            CharacterPoints.Con += conGain;
            CharacterPoints.Str += strGain;
            CharacterPoints.Dex += dexGain;
            MyHealthAttributes.MaxHealth = CharacterPoints.Con * 10;
            MyHealthAttributes.Health = MyHealthAttributes.MaxHealth;

            GuiElements.LevelUpTextStats.text = "Health restored! \n "
                + "STR: +" + strGain + "\n"
                + "DEX: +" + dexGain + "\n"
                + "CON: +" + conGain;
            GuiElements.HealthSlider.value = 1;
            transform.FindChild("LevelUp").GetComponent<ParticleSystem>().Play();
            GuiElements.LevelUpText.enabled = true;
            GuiElements.LevelUpTextStats.enabled = true;
            transform.FindChild("LevelUp").GetComponent<Light>().enabled = true;
            Time.timeScale = 0.5f;
            StartCoroutine(ShowLevelUpText());
            GuiElements.ExpSlider.value = (CurrentExp / (CurrentLevel * 100));

        }
    }

    void Controll()
    {
        if (Input.GetKeyDown(KeyCode.Q) && WeaponActive)
        {
            WeaponActive = false;
            wepController.gameObject.SetActive(false);
        }
        else if (Input.GetKeyDown(KeyCode.Q) && !WeaponActive)
        {
            WeaponActive = true;
            wepController.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// http://answers.unity3d.com/questions/185346/rotate-object-to-face-mouse-cursor-for-xz-based-to.html -Mox
    /// </summary>
    void LookAtMouse()
    {
        if (!Input.GetMouseButton(1))
        {
            // Generate a plane that intersects the transform's position with an upwards normal.
            var playerPlane = new Plane(Vector3.up, transform.position);
            // Generate a ray from the cursor position
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // Determine the point where the cursor ray intersects the plane.
            // This will be the point that the object must look towards to be looking at the mouse.
            // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
            //   then find the point along that ray that meets that distance.  This will be the point
            //   to look at.
            var hitdist = 0.0f;
            // If the ray is parallel to the plane, Raycast will return false.
            if (playerPlane.Raycast(ray, out hitdist))
            {
                // Get the point along the ray that hits the calculated distance.
                var targetPoint = ray.GetPoint(hitdist);
                // Determine the target rotation.  This is the rotation if the transform looks at the target point.
                var targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
                // Smoothly rotate towards the target point.
                if (WeaponActive)
                {
                    if (wepController == null)
                    {
                        Debug.LogError("Active weapon without a prefab error!");
                    }
                    else
                    {
                        var calculatedTurnRate = wepController.TurnRate + (CharacterPoints.Dex / 10);
                        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, calculatedTurnRate * Time.deltaTime);
                    }
                }
                else
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 1);
                }
            }
        }
    }

    float currentX = 0;


    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Enemy weapon")
        {
            other.transform.parent.gameObject.GetComponent<EnemyController>().MeleeHittedPlayer(this);
        }
        if (other.tag == "Dropped weapon" && Input.GetKeyDown(KeyCode.E))
        {
            GameObject newWeapon = null;
            foreach (Transform child in transform.transform)
            {

                if (child.tag == "Player weapon")
                {
                    newWeapon = Instantiate(other.GetComponent<DropController>().TurnIntoThis, child.transform.position, transform.rotation) as GameObject;
                    newWeapon.transform.Rotate(new Vector3(0, 1, 0), other.GetComponent<DropController>().ExtraRotationOnPickUp_X);
                    newWeapon.transform.Rotate(new Vector3(1, 0, 0), other.GetComponent<DropController>().ExtraRotationOnPickUp_Y);
                    newWeapon.transform.Rotate(new Vector3(0, 0, 1), other.GetComponent<DropController>().ExtraRotationOnPickUp_Z);
                    Instantiate(child.GetComponent<WeaponController>().TurnIntoThis, transform.position, Quaternion.identity);
                    Destroy(child.gameObject);
                    Destroy(other.gameObject);
                    print(child.name);
                }
            }
            newWeapon.transform.SetParent(gameObject.transform);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "InnerEffect")
        {
            RenderSettings.fogColor = new Color(0.5f, 0.1f, 0.1f);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "InnerEffect")
        {
           // RenderSettings.fogColor = new Color(0,0,0);
        }
    }

    public void HittedInMelee(float damage)
    {
        transform.FindChild("MeleeHittedBloodSplash").GetComponent<ParticleSystem>().Play(false);
        MyHealthAttributes.Health -= damage;
        GuiElements.HealthSlider.value = MyHealthAttributes.Health / MyHealthAttributes.MaxHealth;
    }

    public void HittedBySpell(float damage)
    {
        MyHealthAttributes.Health -= damage;
        GuiElements.HealthSlider.value = MyHealthAttributes.Health / MyHealthAttributes.MaxHealth;
    }

    public void AddExp(float exp)
    {
        CurrentExp += exp;
        GuiElements.ExpSlider.value = (CurrentExp / (CurrentLevel * 100));


    }

    public void SetOnFire(int dmg, float duration)
    {
        StartCoroutine(FireDamage(dmg, duration));
    }
    public void Poison(int dmg, float duration)
    {
        StartCoroutine(FireDamage(dmg, duration));
    }

    IEnumerator ShowLevelUpText()
    {
        yield return new WaitForSeconds(0.75f);
        GuiElements.LevelUpText.enabled = false;
        GuiElements.LevelUpTextStats.enabled = false;
        transform.FindChild("LevelUp").GetComponent<Light>().enabled = false;
        Time.timeScale = 1.0f;
    }

    IEnumerator FireDamage(int dmg, float duration)
    {
        int i = 0;
        while (i < duration)
        {
            i++;
            MyHealthAttributes.Health -= dmg;
            GuiElements.HealthSlider.value = MyHealthAttributes.Health / MyHealthAttributes.MaxHealth;
            yield return new WaitForSeconds(1.0f);
        }
    }

    public void DisableControll(int sec)
    {
        StartCoroutine(DisableCorutine(sec));
    }

    IEnumerator DisableCorutine(int sec)
    {
        CanMove = false;
        int i = 0;
        while (i < sec)
        {
            yield return new WaitForSeconds(1.0f);
        }
        CanMove = true;
    }
}
