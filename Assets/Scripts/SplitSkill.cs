﻿using UnityEngine;
using System.Collections;

public class SplitSkill : MonoBehaviour {

    public GameObject SplitInto;
    public int SplitsTo;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(GetComponent<HealthController>().Health <= 0)
        {
            Vector3 diff = new Vector3(1, 0, 0);
            for (int i = 0; i < SplitsTo; i++)
            {
                diff = Quaternion.Euler(0, 360 / SplitsTo, 0) * diff;


                Instantiate(SplitInto, transform.position + diff, Quaternion.identity);
                this.enabled = false;
            }
        }
	}
}
